---
Title: Cobbled Paths
illustration: cobbled-paths-2.svg
Meta:
    Date: 8 June 2024 09:00 - 18:00
    Place: Meyboom, 34 Pachecolaan, Bruxelles
    Price: free
    Places: 8 to 16
    Reservation: miam@osp.kitchen
    Deadline: 30th of May
    OSP: Doriane, Ludi
---

For this first Plotter Station workshop, OSP invites you on a griddy and tortuous adventure of making collective pen-plotted poster using their DIY tool named _Cobbled Paths_.

_Cobbled Paths_ is a web interface that brings multiple tools together to allow experimental and direct collaboration on pen-plotted drawings through the making of [Ascii art](https://en.wikipedia.org/wiki/ASCII_art) on [etherpads](https://etherpad.org/).

<div class="gallery" markdown>

![](/images/agenda/cp_002.jpg 'cobbled path workshop in ISBA in Besançon during march 2024')

![](/images/agenda/cp_001.jpg 'cobbled path workshop in ISBA in Besançon during march 2024')

![](/images/agenda/cp_000.jpg 'cobbled path workshop in ISBA in Besançon during march 2024')

</div>

The first step is in Ascii art fashion, to draw shapes and lines using Ascii characters such as `( ) / \ | ' - . _ = +`  on etherpads. An SVG vector interpretation of the shapes and lines is rendered in a side view, that can later be translated to HPGL to draw them with a pen plotter using normal pens.

<div class="gallery" markdown>

![](/images/agenda/cp_003.jpg 'view of the cobbled path interface')

![](/images/agenda/cp_004.png 'view of the cobbled path interface')

</div>

It makes a way from the blocky discontinuity of Ascii drawings,
to the smoothness of bezier curves,
to an anologic pen-plotter interpretation.

Like cobbled paths, none of these technologies are new, and we've been walking on them for years.
Like cobbled paths, it is a reminder of the permeability between the discrete and the continuous; how regular stones can form tortuous paths.


## Practical

The workshop is hosted at [Meyboom, 34 Pachecolaan](https://meyboom.space/#studio-7) in OSP's studio.

It is divided in two parts

In the morning we will introduce our Ascii 2 Vector 2 Plotter pipeline. By making a first collaborative poster we'll explore the materiality of Ascii art, its grid and translation to SVG.
This is a playground: copy and paste found Ascii art, start drawing letters, explore patterns and diagrams and remix each others drawings.

In the afternoon we will split into smaller groups, depending the shared interest of the participants, we'll pick a theme for all posters.
Each group will work on their own poster, this time participants will be invited to:
   
- use [FIGlet](http://www.figlet.org/) fonts to collaboratively compose a typographic poster on a two dimensional grid
- switch pens colors or pen brushes by dividing the poster in different layers
- experiment with smaller and bigger shapes as a way to control the relative thickness of the pen

## Reservation

We look for a diversity of practice, background, level of amateurism, gender and age.
No knowledge (other than a desire to draw with Ascii characters) is required to participate.
As one of the ideas behind the plotter-station is to make available our machines and create knowledge sharing opportunities everybody is more than welcome.

In order to participate please [send us an email](mailto:miam@osp.kitchen) with a short sentence for each of the following:

- a bit on yourself (if you are students or not, where are you coming from, ...)
- your intersest in this workshop
- how it could relate to your (other) practices (doesn't have to be graphic design!)

Deadline is the 30th of May. 
The selection will be published on the 31st. 

Note that the selection process is there to ensure equal opportunity in the case the workshop is overbooked.

