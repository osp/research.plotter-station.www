---
Title: Plotter Party
Illustration: plotter-party-poster-square-scan.jpg
Meta:
    Date: 4 May 2024 15:00 - 19:00
    Place: Meyboom, 34 Pachecolaan, Bruxelles
    Price: free
    OSP: Gijs
    Guests: Victor, Thijs, ???
---

On May 4th OSP and [XPUB](https://xpub.nl/) host a pen plotter party celebrating the launch of the plotter station at OSP's studio in the Meyboom Artist run Spaces. The party is a moment of exchange engaging plotter enthusiasts and anyone curious to explore plotting cultures.

During the party we'll make an interactive tour of pen-plotter experiments, tools and zines: exploring the performative and attitudes of the plotters, vector graphics, illustrations, typography and layout, we'll speak HPGL and share practices working intimately with tools, while using free and open source software.

We warmly invite you, to join us in the tour, or to show your own experiments!

Pen plotters are mechanical drawing machines using 'normal' pens, they were originally developed in the 70's and 80's to make (large format) prints. These plotters are controlled through HPGL, a seductively simple language prescribing the movement of the pens. Compared to modern (large format inkjet) printers, pen plotters are expensive to operate and slow. But it is exactly the slowness, and performative movement that makes them empathic, the simplicity which allows for easy appropriation and their lack of optimization which offers precise control.

With the plotter party, OSP launches their plotting station as a next step in ten years of pen plotter activities, curiosity and enthusiasm. The plotter station gives access to OSP's collection of pen plotters and other printing machines. The plotter station organizes workshops, develops research and can be booked to use the printers.

XPUB is the Experimental Publishing Master course in Fine Art and Design at the Piet Zwart Institute, Rotterdam. XPUB focuses on the acts of making things public in the age of post-digital networks. They have taken a recent interest in pen plotters as a dynamic of (non)obsolete technologies, an attitude of performance and a method of collaboration, and will bring recent explorations and experiments.

- 15:00: Turning the pen-plotters on
- 16:00-18:00: Interactive tour
- 19:00: turning the pen-plotters off

