---
Title: Drawing Curved
Illustration: drawing_curved.png
Meta:
    Date: 2018
    OSP: "Pierre Huyghebaert, Colm O'Neill"
    Collaborators: 'Femke Snelting, Libre Graphic Research Unit'
---

[Drawing curved](http://drawingcurved.osp.kitchen/foreword.xhtml) is a collection of texts and images concerned with digital curvature. It seeks to understand when a curve starts to be tangible, or what might give a sense that they can or can't be handled. What makes a curve smooth? When does it seem appropriate to interrupt it?