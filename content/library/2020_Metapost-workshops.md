---
Title: Metapost workshops
Illustration: metapost.jpg
Meta:
    Date: from 2014 to 2020
    Places: 'ANRT, erg, Maison du livre, OSP studio, Port-au-Prince, Rio'
    OSP: 'Antoine Gelgon, Gijs de Heij, Pierre Huyghebaeart, Alexandre Leray, Ludi Loiseau, Sarah Magnan'
    
---

Series of workshops using the language Metapost to collectively draw a font. Metapost is an open source programming language which allows to draw glyphs by their skeleton or "ductus". The digital or physical pen tracing the skeleton defines the final shape of the glyph. During the workshops a penplotter drew snapshots of the font under construction.

- [Metapost workshop meta repository; tools and infrastructure](http://osp.kitchen/workshop/metapost-workshops/)
- [Metahoguet, 2016](http://osp.kitchen/workshop/metahoguet/)
- [Metapost with ANRT, 2020](http://osp.kitchen/workshop/metapost-anrt/)