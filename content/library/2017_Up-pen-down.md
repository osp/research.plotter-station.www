---
Title: Up-pen-down
Illustration: upanddown.png
Meta:
    Date: 2017
    OSP: 'Gijs de Heij, Pierre Huyghebaert, Ludi Loiseau'
    Collaborators: 'Louise Baduel, Sarah van Lamsweerde, Adva Zakai, '
---

[Up-pen-down](http://osp.kitchen/live/up-pen-down/) was performed by OSP at Balsamine Theater as a part of Saison des Cultures Numériques 2017. This performance was the first public moment of a research focused on that which is between digital type design and bodies. Letters and movements, dance notation and programming, digital codes and coded physical gestures, plotters and body parts interacted with each other and blured the distinction between choreographic and digital practices.
