---
Title: NaturArchy exhibition design
Illustration: 2024-06-imal-posters-2.jpg
Meta:
    Date: 2024
    OSP: "Simon Browne, Gijs de Heij, Clara Pasteau"
    Collaborators: 'iMAL, Roland plotter, Laboratorium'
---

OSP were commissioned by iMAL to design a graphic identity for the exhibition [NaturArchy: Towards a Natural Contract](https://www.imal.org/en/events/naturarchy), taking place in Brussels, from the 25.05 till 29.09.2024.

As the exhibition is comprised of art/science collaborations on the theme of nature, we began with an intention to experiment with pen plotters and bio inks. The bio inks were previously used in a [2018 collaboration](https://plotterstation.osp.kitchen/library.html#colorlab) between OSP and María Boto Ordonez, a scientist working at the [Laboratorium](http://laboratorium.bio).

For a report on the process, please see [this post](http://blog.osp.kitchen/works/naturarchy-exhibition-design.html) on the OSP blog.