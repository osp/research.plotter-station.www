---
Title: 16 cases story
Illustration: 16casesstory.png
Meta:
    Date: 2012
    OSP: "Colm O'Neill, Femke Snelting, Gijs de Heij, Pierre Huyghebaert, Pierre Marchand"
    Collaborators: 'Active Archive tool, Roland plotter, Ministry of culture of the Flemish Community'
---

A catalog of case-stories from the parctice of lay-out, even if this practice is sometimes imaginary. The stories were selected after many discussions, workshops and brainstorms because they are each in their own way an invitation to rethink lay-out from scratch.
A facsimile can be found [here on our gitlab](https://gitlab.constantvzw.org/osp/work.stories/-/raw/archive/16_case_stories_re-imagining_the_practice_of_layout/facsimile_digital-catalog.pdf?ref_type=heads).