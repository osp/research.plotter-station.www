Title: Plot With Us
Lang: en

## Workshops

The plotter station organizes public events and workshops to get to know the plotters, learn how to use them and for specific research. Workshops are announced on the [agenda page](/category/agenda.html).

## Use the plotters

If you want to plot using the plotters you can reserve a time-slot, please send an email to <miam@osp.kitchen> to book an appointment.

Don't hesitate to contact us if you have a question, an educational project or a research project linked to the station.

To prepare for your visit, here's some practical information.

The plotter station is hosted by [OSP](https://osp.kitchen).
OSP workspace is part of [Meyboom artist-run-spaces](https://meyboom.space/), located at Boulevard Pachécolaan 34-2, 1000 Brussels.
Meyboom brings together a community of individuals and collectives working in different fields that has continually evolved since its formation in the year 2013.

The workspace is located on the 2nd floor of a building equipped with elevators, one of the access doors has a ramp. The floor has wheelchair accessible bathrooms without handrails. Public transport is a few hundred meters away (Botanique/Kruidtuin, Rogier, Central station, Brussels Congres). 
The building has bike racks behind a lockable door.

## Code of conduct

### A meeting space

The plotter station is a self managed space.
The space is dedicated to sharing knowledge and to provide access to a collection of tools, both hard- and software.
A space where we search, debate and experiment together. We appreciate the path of the projects more than the result itself.
It is a space that brings together different people, with different experiences and expertises.
In that sense it's also a meeting space where you can bring even more than what you can find.
The interaction with the other inhabitants, of the station and the floor, [Meyboom Artist Run Spaces](https://meyboom.space/), allows for encounters with other practices.
Being situated within Meyboom Artist Run Spaces, we ask to respect its Internal Organization Agreement (IOA) which includes the following guidelines :

> Be conscious of differences in identities and experiences. Be mindful of how you take up space both in meetings but also more generally within the community, also in relation to your gender identity and background.

> We do not tolerate any racism, sexism, agism, ableism, LGBTQ+ phobia, fatphobia etc. in our space. 

### About making

There is no requirement for you to present your project. 

The station offers different interfaces, or entry doors, meant to allow you to enage with our infrastructure, whatever your skill level.

The plotter station allows you to put the hands in the machine, to try, to fail and to do differently.
It brings together machines to draw, not people to plot stuff for you.
While the dialogue with the plotter station can sometimes go through the command line you are not here to place an order.
 
To define this Code of Conduct we borrow [collaboration guidelines](https://constantvzw.org/wefts/orientationspourcollaboration.en.html) from Constant, which asks collaborators to take the following into account:

> Welcoming multiple processes of (un)learning. The exchange of information, experience and knowledge comes in many forms.

> Accepting differences. Appreciating divergence in pace, points of view, backgrounds, references, needs and limits.

> Recognizing that words and ways of speaking impact people in various ways.

> Caring for language gaps. This is a multi-lingual environment. Using Free, Libre and Open Source software whenever possible.

> Asking for explicit consent before sharing photographs or recordings on proprietary social networks.

> The default license for all material and documentation generated during the worksession is the Collective Conditions for re-Use,CC4r.

> Knowing that taking all of the above into account is sometimes easier said than done.


### On content

Here we took inspiration from the [charte by Atelier du Toner](http://ateliersdutoner.com/news/notre-charte-du-toner).
<!--from Toner guidelines -->

<!-- Ne serons tolérés aucune propagande raciste, sexiste, transphobe, grossophobe, validiste ou homophobe.-->
> Content edited with the plotterstation may not include any racism, sexism, agism, ableism, LGBTQ+ phobia, fatphobia etc.

<!--from Toner guidelines -->

<!--On y imprime son propre travail, que ce soit en tant qu’ auteurice, ou éditeurice, en tant que collectif, que créateurice de revue, illustrateurice, amateurice ou curieu·x·ses …-->

The plotterstation is meant to be used by authors, publishers, collectives, creators, artists, illustrators, amateurs, or enthusiast, to plot their own work.



###  Taking care of the space
<!-- Les machines présentes dans les ateliers sont laissées à disposition dans un esprit de partage. Elles devront être utilisées avec précaution et respect.
> Il est également demandé aux utilisateurices de l’atelier de ranger derrière elleux, et laisser les lieux dans un état encore meilleur que celui dans lequel iels l’ont trouvé.-->

We open access to this space with the intention to cultivate curiosity and encourage experimentation, though, plotters are very delicate machines and need to be handled with care. And, as intense plotting session can involve clutter, be cautious to tidy the area after using it.



<!-- L’atelier du Toner comprend également une bibliothèque, où l’on peut consulter les ouvrages qui ont déjà été réalisés sur place. Les utilisateurices de l’atelier s’engagent à alimenter cette bibliothèque en y laissant 1 exemplaire de leur ouvrage une fois fini.-->


The plotterstation's website host archives of what has been done here, by using the space you agree to share the outcome of your projects on the website. Also if you discovered a new exiting way to use the plotter, feel free to include a recipe, readme or a documentation with the community.

