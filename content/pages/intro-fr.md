Title: About
Lang: fr
Translation: true

La station de traçage est un projet nous fournit un contexte pour développer l'expérimentation artistique, déplier les questions esthétiques liées à nos pratiques et performer l'impression.
L’installation permet de sensibiliser et d’accueillir un public en organisant des événements publics tels que des ateliers, résidences et performances, en partageant les connaissances et en donnant accès à une collection d'outils, à la fois matériels et logiciels.

Un plotter ou traceur est une machine à dessiner mécanique utilisant des stylos “communs”. Développée dans les années 70 et 80 elle est utilisée à l’époque pour des impressions grand format de plans d’architecture, de dessins DAO ou de graphiques d’entreprise. Elles sont contrôlées par un ordinateur au moyen d'un langage d’une simplicité séduisante, le HPGL,  qui décrit le mouvement des stylos. Bien que les traceurs de découpe contemporains soient basés sur ces mêmes machines, la lenteur, le coût d’utilisation et les imprimantes à jet d’encre grand format ont rendu ces premiers traceurs obsolètes. Mais c’est justement cette lenteur et ce mouvement performatif qui les rendent empathiques. Alors que leur simplicité permet une appropriation facile, leur mouvement non optimisé et répétitif donne un aperçu des commandes qu’ils reçoivent de l’ordinateur. Comme un traceur dessine avec un stylo, il dessine des lignes plutôt que des aplats, la forme de ces lignes est influencée par le stylo, son épaisseur, son angle et l’encre. Les traceurs et leurs gestuelles produisent aussi en ensemble de sons, autres voix potentielles pour entrer en dialogue. Pour toutes ces raisons ce sont d’excellents compagnons pour nos recherches.

Notre proposition est motivée par la conviction collective qu'à travers des expériences spécifiques, OSP a accumulé des savoirs-faires singuliers et marginaux, qui méritent d'être partagés.
L'intention étant de développer la station à travers une recherche artistique et technique basée sur notre propre pratique et en sollicitant un réseau d’experts, ainsi que de la rendre disponible et de la diffuser à d’autres praticien·nes visuel·les à Bruxelles, en Belgique et à l’étranger.

Ces deux intentions - la manière dont le savoir technique d'OSP influence les pratiques et comment celui-ci est en retour nourri des usages - s’entrecroisent et se renforcent mutuellement, comme tend à le montrer le graphique suivant.

![](https://cloud.osp.kitchen/s/tAQZAtDsabqwZ3W/preview "Schéma de la station de traçage comme œuvre de recherche, généré avec Graphviz, 2023")

Faire “un travail graphique professionnel utilisant uniquement des outils open-source” a rapidement signifié plier les outils (logiciels) existants et en créer de nouveaux. Pour OSP, la fabrication d’outils est autant un geste artistique que le travail produit en les utilisant. Ce lien d'intimité avec nos outils tente de rompre avec l’hégémonie des logiciels propriétaires normalisés, prenant en compte le fait qu'il n'y a pas qu'une seule manière de faire, et que nos outils reflètent déjà en eux-mêmes des formes graphiques et culturelles. 



![Federation Wallonia-Brussels logo](https://cloud.osp.kitchen/s/tAQZAtDsabqwZ3W/preview)




Le projet en est à sa première étape. Pour la mise en place de cette première version du site web et la programmation d'une première série de workshops à venir en 2024 et 2025, OSP a bénéficié d'un soutien de La Fédération Wallonie Bruxelles, Aide pour l'organisation d'un événement et l'acquisition de matériel à hauteur de 4950 euros.
