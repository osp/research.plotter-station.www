Title: Resources

## Tutorials and documentations

<http://relearn.be/2013/r/cheat-sheet::using-the-plotter.html>
:   Guide on how to connect to a plotter.

<http://www.isoplotec.co.jp/HPGL/eHPGL.htm>
:   Overview of HPGL commands and syntax.

<https://plot.heij.be/code-hpgl.html>
:   (Interactive) example drawings

<https://gitlab.constantvzw.org/osp/work.colorlab/-/blob/master/adapters/stabilo-adapter-simple.stl>
:   Stabilo adapter, printable stl

<https://gitlab.constantvzw.org/osp/work.colorlab/-/blob/master/adapters/stabilo-adapter-simple.stl>
:   Stabilo adapter, editable [OpenSCAD](https://openscad.org/) file


## Other stations

### Xpub

Teachers at [Xpub](https://xpub.nl/) started to inspect and repair all plotters they could find in the school. 
This has triggered a lot of plotter enthusiasm with the students, who made cross-hatching software, vertical alignment tools, pen holders, generated plotter mazes, zines, writing experiments, 3d to plot scripts, and there is probably more. 
More information can be found on their wiki, [Plotters at XPUB](https://pzwiki.wdka.nl/mediadesign/Pen_plotters#Plotters_at_XPUB), on which you can also find unique pen plotter memes!

<!-- 

### Rosi

[Chez rosi](https://chezrosi.wordpress.com/) is a "cool & artisanal" risograph studio bruxelles. Work with a great care for quality and understanding of the technology. Eager for experiment.

-->