---
Title: HP DraftMaster II
Illustration: HP-DraftMaster_osp1.png, HP-DraftMaster_add.jpg
Technical_drawing : HP-DraftMaster_manual1.jpg
size: 3
Meta:
    Year of introduction: 1987
    Machine dimensions: 1200mm(H) x 1346mm(W) x 508mm(D)
    Weight: 73kg
    Max. plotting area:
    Max. plotting speed: 60.0 cm/s
    Max. Paper size: 36 x 48in. (Architectural E) / A0
    Paper holding: Rollers
    Distance accuracy: 0.09% of the move or 0.25mm, whichever is greater.
    Repeat accuracy: 0.1mm
    Right angle accuracy:
    Display LED:
    Buffer Size: 25K (configurable)
    Interface:
    Number of pens: 8
    Maximum pen height:
    Pen force: 15-66 grams
    Machine operating style: Paper moves in Y, pen in X
    Multipages: No
    State:
    entry doors:
---
