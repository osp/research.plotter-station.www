---
Title: Silhouette Cameo 3
Illustration: Cameo_osp1.jpg, Cameo4_add.jpg 
Technical_drawing : Cameo3_manual.png
size: 0
Meta:
    Year of introduction: 2016
    Machine dimensions: 57 cm x 22 cm x 15 cm
    Weight: 4.053kg
    Max. plotting area: 304.8mm x 304.8mm on mat, 8in x 10ft on roll
    Max. plotting speed:
    Max. Paper size: 8in x 10ft
    Max. Paper Thickness: 2mm
    Paper holding: Rollers
    Distance accuracy:
    Repeat accuracy:
    Right angle accuracy:
    Display LED:
    Data buffer:
    Interface:
    Number of pens: 1
    Maximum pen height:
    Pen force: up to 210gf
    Machine operating style: Paper moves in Y, pen in X
    Multipages: no
    State:
    entry doors:
---
