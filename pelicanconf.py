AUTHOR = 'OSP'
SITENAME = 'Plotter Station'
DESCRIPTION = 'An initiative of Open Source Publishing to gives access to their collection of pen-plotters, organizes workshops, and develops research.'
SITEURL = 'https://plotterstation.osp.kitchen'

PATH = 'content'
TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = 'en'
THEME = 'theme'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

USE_FOLDER_AS_CATEGORY = True

DIRECT_TEMPLATES = ['index']

DEFAULT_DATE = 'fs'

# disable the generation of those default html files
AUTHOR_URL = AUTHOR_SAVE_AS = ''
TAG_URL = TAG_SAVE_AS = ''

CATEGORY_URL = CATEGORY_SAVE_AS = '{slug}.html'

# disable article generation since they show inside of category pages
ARTICLE_SAVE_AS = ''
ARTICLE_URL = "{category}.html#{slug}"

PLUGIN_PATHS = ['plugins']
PLUGINS = ['yaml_metadata']

# MD EXTENSION
# ==============================================================================

import yafg
# https://pypi.org/project/yafg/

# from markdown_figcap import FigCapExtension
# https://github.com/funk1d/markdown-figcap

MARKDOWN = {
    'extensions': [
        yafg.YafgExtension(stripTitle=False)
    ],
    'extension_configs': {
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.toc': {},
    },
    'output_format': 'html5',
}

# for officially and third party extensions:
# https://python-markdown.github.io/extensions/